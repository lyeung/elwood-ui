# elwood-ui

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

### Project Home ###

* https://www.hostedredmine.com/projects/elwood-ui

### Project Wiki ###

* https://www.hostedredmine.com/projects/elwood-ui/wiki

### Project Blog ###

* http://tamemymonkeymind.blogspot.com.au/search/label/elwood

### Issue Management ###

* https://www.hostedredmine.com/projects/elwood-ui/issues

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

